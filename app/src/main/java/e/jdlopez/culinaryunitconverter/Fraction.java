package e.jdlopez.culinaryunitconverter;

import java.math.BigInteger;

/**
 *
 * Handles math operations between fractions
 *
 * @author jdlopez
 */
public class Fraction {

    /**
     * The top number of the fraction
     */
    private long numerator;

    /**
     * The bottom number of the fraction
     */
    private long denominator;

    /**
     * Default constructor
     */
    public Fraction(){
        this(0,1);
    }

    /**
     * Constructor given numerator and denominator
     * @param numerator Numerator
     * @param denominator Denominator
     */
    public Fraction(long numerator, long denominator){
        this.numerator = numerator;
        this.denominator = denominator;
    }

    /**
     * Constructor for converting a double to a fraction
     * @param num
     */
    public Fraction(long num){
        this(num + "");
    }

    /**
     * Construcor for converting a string to a fraction
     * @param num
     */
    public Fraction(String num){
        this();

        String[] number = num.split("[.]");
        int len = number.length;

        /* If the num isn't a double */
        if(len == 2){
            long numOfDecimals = 10 * Integer.parseInt(number[1]);

            setNumerator((long) Double.parseDouble(num) * numOfDecimals);
            setDenominator(numOfDecimals);

        }else {
            setNumerator(Integer.parseInt(num));
            setDenominator(1);
        }
    }

    /**
     * Getter method for numerator
     * @return numerator
     */
    public long getNumerator(){
        return numerator;
    }

    /**
     * Getter method for denominator
     * @return denominator
     */
    public long getDenominator(){
        return denominator;
    }


    /**
     * Set the numerator to this new number
     * @param set
     */
    public void setNumerator(long set){
        this.numerator = set;
    }

    /**
     * Set the denominator to this new number
     * @param set
     */
    public void setDenominator(long set){
        this.denominator = set;
    }

    /**
     * Multiply current fraction with frac
     * @param frac the fraction to be multiplied
     * @return new fraction
     */
    public Fraction multiply(Fraction frac){
        long aTop = this.getNumerator();
        long bTop = frac.getNumerator();

        long aBottom = this.getDenominator();
        long bBottom = frac.getDenominator();

        long newTop = aTop * bTop;
        long newBottom = aBottom * bBottom;
        Fraction c = new Fraction(newTop, newBottom);

        return c.simplify(c.findGCD());
    }

    /**
     * Divide current fraction with frac
     * @param frac the fraction to be divided
     * @return new fraction
     */
    public Fraction divide(Fraction frac){
//        int aTop = this.getNumerator();
        long bTop = frac.getNumerator();
//
//        int aBottom = this.getDenominator();
        long bBottom = frac.getDenominator();
//
//        if(aBottom == bBottom)
//            this.answer = newFraction();


        return this.multiply(new Fraction(bBottom, bTop));
    }

    /**
     * Find the greatest common denominator
     * @return New simplified fraction
     */
    private long findGCD(){
        long tempTop = this.numerator;
        long tempBottom = this.denominator;

        long temp = 0;
        while (tempBottom != 0){
            temp = tempBottom;
            tempBottom = tempTop % tempBottom;
            tempTop = temp;
        }

        return tempTop;
    }

    /**
     * Makes the smallest numerator and denominator possible
     * @param gcd the greatest common factor
     */
    private Fraction simplify(long gcd){
        return new Fraction(this.numerator /= gcd,this.denominator /= gcd);
    }

    @Override
    public String toString(){
        long top = this.getNumerator();
        long bottom = this.getDenominator();
        long wholeNumber = top / bottom;
        long fraction = top % bottom;

        String number = "";

        if(wholeNumber !=  0)
            number += wholeNumber + " ";
        if(fraction != 0)
            number += "(" + fraction + "/" + bottom + ") ";

        return number;
    }
}
