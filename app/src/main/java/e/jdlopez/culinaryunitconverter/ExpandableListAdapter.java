package e.jdlopez.culinaryunitconverter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private final String TEASPOON = "teaspoon";
    private final String TABLESPOON = "tablespoon";
    private final String FLUID_OUNCE = "fluid ounce";
    private final String GILL = "gill";
    private final String CUP = "cup";
    private final String PINT = "pint";
    private final String QUART = "quart";
    private final String GALLON = "gallon";
    private final String ML = "ml";
    private final String L = "l";
    private final String DL = "dl";

    private final String POUND = "pound";
    private final String OUNCE = "ounce";
    private final String G = "g";
    private final String MG = "mg";
    private final String KG = "kg";

    private final String MM = "mm";
    private final String CM = "cm";
    private final String M = "m";
    private final String INCH = "inch";
    private final String FEET = "feet";

    private final String FAHRENHEIT = "fahrenheit";
    private final String CELSIUS = "celsius";
    private final String GAS_MARK = "gas mark";
    private final String VERBAL = "verbal";

    private final Set<Pair<Long, Long>> mCheckedItems = new HashSet<>();

    private Context context;
    /**
     * header titles
     */
    private List<String> listDataHeader;
    /**
     * Child data in format of header title, child title
     */
    private HashMap<String, List<String>> listDataChild;

    /**
     * Constructor for Expandable List Adapter class
     *
     * @param context the parent context
     * @param listDataHeader header titles
     * @param listChildData  Child data in format of header title, child title
     */
    ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return Objects.requireNonNull(this.listDataChild.get(this.listDataHeader.get(groupPosition))).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert infalInflater != null;
            convertView = infalInflater.inflate(R.layout.unit_item, null);
        }

        final CheckBox cb = convertView.findViewById(R.id.unit_list_item);
        // add tag to remember groupId/childId
        final Pair<Long, Long> tag = new Pair<>(
                getGroupId(groupPosition),
                getChildId(groupPosition, childPosition));
        cb.setTag(tag);
        // set checked if groupId/childId in checked items
        cb.setChecked(mCheckedItems.contains(tag));
        // set OnClickListener to handle checked switches

        cb.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                final CheckBox cb = (CheckBox) v;
                final Pair<Long, Long> tag = (Pair<Long, Long>) v.getTag();
                RelativeLayout rL;
                String temp = cb.getText().toString();
                int id = 0;

                switch (temp) {
                    case TEASPOON:
                        id = R.id.teaspoon_relative_layout;
                        break;
                    case TABLESPOON:
                        id = R.id.tablespoon_relative_layout;
                        break;
                    case FLUID_OUNCE:
                        id = R.id.fluid_ounce_relative_layout;
                        break;
                    case GILL:
                        id = R.id.gill_relative_layout;
                        break;
                    case CUP:
                        id = R.id.cup_relative_layout;
                        break;
                    case PINT:
                        id = R.id.pint_relative_layout;
                        break;
                    case QUART:
                        id = R.id.quart_relative_layout;
                        break;
                    case GALLON:
                        id = R.id.gallon_relative_layout;
                        break;
                    case ML:
                        id = R.id.ml_relative_layout;
                        break;
                    case L:
                        id = R.id.l_relative_layout;
                        break;
                    case DL:
                        id = R.id.dl_relative_layout;
                        break;
                    case POUND:
                        id = R.id.pound_relative_layout;
                        break;
                    case OUNCE:
                        id = R.id.ounce_relative_layout;
                        break;
                    case G:
                        id = R.id.g_relative_layout;
                        break;
                    case MG:
                        id = R.id.mg_relative_layout;
                        break;
                    case KG:
                        id = R.id.kg_relative_layout;
                        break;
                    case MM:
                        id = R.id.mm_relative_layout;
                        break;
                    case CM:
                        id = R.id.cm_relative_layout;
                        break;
                    case M:
                        id = R.id.m_relative_layout;
                        break;
                    case INCH:
                        id = R.id.inch_relative_layout;
                        break;
                    case FEET:
                        id = R.id.feet_relative_layout;
                        break;
                    case FAHRENHEIT:
                        id = R.id.fahrenheit_relative_layout;
                        break;
                    case CELSIUS:
                        id = R.id.celsius_relative_layout;
                        break;
                    case GAS_MARK:
                        id = R.id.gas_mark_relative_layout;
                        break;
                    case VERBAL:
                        id = R.id.verbal_relative_layout;
                        break;
                }
                rL = ((Activity) context).findViewById(id);

                if (cb.isChecked()) {
                    mCheckedItems.add(tag);
                    if (rL != null)
                        rL.setVisibility(View.VISIBLE);
                } else {
                    mCheckedItems.remove(tag);
                    if (rL != null)
                        rL.setVisibility(View.GONE);
                }
            }
        });

        final TextView unitListChild = convertView.findViewById(R.id.unit_list_item);
        unitListChild.setText(childText);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return Objects.requireNonNull(this.listDataChild.get(this.listDataHeader.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert infalInflater != null;
            convertView = infalInflater.inflate(R.layout.unit_group, null);
        }

        TextView unitListHeader = convertView.findViewById(R.id.unit_list_header);
        unitListHeader.setTypeface(null, Typeface.BOLD);
        unitListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public Set<Pair<Long, Long>> getCheckedItems() {
        return mCheckedItems;
    }
}