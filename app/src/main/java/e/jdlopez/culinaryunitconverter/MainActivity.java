package e.jdlopez.culinaryunitconverter;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private final int NUM_OF_VOLUME_UNITS = 11;
    private final int NUM_OF_MASS_AND_WEIGHT_UNITS = 5;
    private final int NUM_OF_LENGTH_UNITS = 5;
    private final int NUM_OF_TEMPERATURE_UNITS = 4;

    private final String TEASPOON = "teaspoon";
    private final String TABLESPOON = "tablespoon";
    private final String FLUID_OUNCE = "fluid ounce";
    private final String GILL = "gill";
    private final String CUP = "cup";
    private final String PINT = "pint";
    private final String QUART = "quart";
    private final String GALLON = "gallon";
    private final String ML = "ml";
    private final String L = "l";
    private final String DL = "dl";

    private final String POUND = "pound";
    private final String OUNCE = "ounce";
    private final String G = "g";
    private final String MG = "mg";
    private final String KG = "kg";

    private final String MM = "mm";
    private final String CM = "cm";
    private final String M = "m";
    private final String INCH = "inch";
    private final String FEET = "feet";

    private final String FAHRENHEIT = "fahrenheit";
    private final String CELSIUS = "celsius";
    private final String GAS_MARK = "gas mark";
    private final String VERBAL = "verbal";

    /**
     * 0:  teaspoon
     * 1:  tablespoon
     * 2:  fluid ounce
     * 3:  gill
     * 4:  cup
     * 5:  pint
     * 6:  quart
     * 7:  gallon
     * 8:  ml
     * 9:  l
     * 10: dl
     */
    public TextView[] volumeArray;

    /**
     * 0: pound
     * 1: ounce
     * 2: mg
     * 3: g
     * 4: kg
     */
    public TextView[] massAndWeightArray;

    /**
     * 0: mm
     * 1: cm
     * 2: m
     * 3: inch
     * 4: feet
     */
    public TextView[] lengthArray;

    /**
     * 0: fahrenheit
     * 1: celsius
     * 2: gas mark
     * 3: verbal
     */
    public TextView[] temperatureArray;

    /**
     * The unit to convert from
     */
    String baseUnit = "";

    /**
     * 0:  gallon
     * 1:  quart
     * 2:  pint
     * 3:  cup
     * 4:  tablespoon
     * 5:  teaspoon
     * 6:  ml
     * 7:  l
     * 8:  dl
     * 9:  fluid ounce
     * 10: gill
     */
    Unit[] unitVolumeArray = new Unit[NUM_OF_VOLUME_UNITS];

    /**
     * 0: pound
     * 1: ounce
     * 2: g
     * 3: mg
     * 4: kg
     */
    Unit[] unitMassAndWeightArray = new Unit[NUM_OF_MASS_AND_WEIGHT_UNITS];

    /**
     * 0: mm
     * 1: cm
     * 2: m
     * 3: inch
     * 4: feet
     */
    Unit[] unitLengthArray = new Unit[NUM_OF_LENGTH_UNITS];

    /**
     * 0: fahrenheit
     * 1: celsius
     * 2: gas mark
     * 3: verbal
     */
    Unit[] unitTemperatureArray = new Unit[NUM_OF_TEMPERATURE_UNITS];
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    Button submitBtn;
    Spinner spinner;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    /**
     * The number to convert
     */
    private EditText userInputEditText;
    /**
     * This keeps track where to start the conversion chain array
     */
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();

        constructArray();

        constructUnitArray();

        prepareListData();

        handleAllSetOn();
    }

    private void findViews() {
        submitBtn = findViewById(R.id.submit_btn);
        spinner = findViewById(R.id.origin_unit_spinner);
        userInputEditText = findViewById(R.id.user_input_edit_text);
        expListView = findViewById(R.id.units_menu);
    }

    private void handleAllSetOn() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.unit, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                baseUnit = adapterView.getItemAtPosition(i).toString();

                LinearLayout lLVolume = findViewById(R.id.volume_linear_layout);
                LinearLayout lLMassAndWeight = findViewById(R.id.mass_and_weight_linear_layout);
                LinearLayout lLLength = findViewById(R.id.length_linear_layout);
                LinearLayout lLTemperature = findViewById(R.id.temperature_linear_layout);

                switch (baseUnit) {
                    case TEASPOON:
                        index = 0;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case TABLESPOON:
                        index = 1;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case FLUID_OUNCE:
                        index = 2;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case GILL:
                        index = 3;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case CUP:
                        index = 4;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case PINT:
                        index = 5;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case QUART:
                        index = 6;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case GALLON:
                        index = 7;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case ML:
                        index = 8;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case L:
                        index = 9;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case DL:
                        index = 10;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case POUND:
                        index = 0;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.VISIBLE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case OUNCE:
                        index = 1;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.VISIBLE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case G:
                        index = 2;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.VISIBLE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case MG:
                        index = 3;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.VISIBLE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case KG:
                        index = 4;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.VISIBLE);
                        lLLength.setVisibility(View.GONE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case MM:
                        index = 0;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case CM:
                        index = 1;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case M:
                        index = 2;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case INCH:
                        index = 3;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case FEET:
                        index = 4;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case FAHRENHEIT:
                        index = 0;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case CELSIUS:
                        index = 1;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case GAS_MARK:
                        index = 2;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    case VERBAL:
                        index = 3;
                        lLVolume.setVisibility(View.GONE);
                        lLMassAndWeight.setVisibility(View.GONE);
                        lLLength.setVisibility(View.VISIBLE);
                        lLTemperature.setVisibility(View.GONE);
                        break;
                    default:
                        index = 0;
                        lLVolume.setVisibility(View.VISIBLE);
                        lLMassAndWeight.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                baseUnit = TEASPOON;
                index = 0;
            }
        });

        expListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + Objects.requireNonNull(listDataChild.get(
                                listDataHeader.get(groupPosition))).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return true;
            }
        });

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);

        userInputEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitBtn.performClick();
                    return true;
                }
                return false;
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userInputEditText.getText().toString().trim().equals(""))
                    userInputEditText.setText("0");
                if (isVolume())
                    convertVolume();
                else if (isMassAndWeight())
                    convertMassAndWeight();
                else if (isLength())
                    convertLength();
                else if (isTemperature())
                    convertTemperature();
                else
                    Toast.makeText(getApplicationContext(), "Please enter a valid number for that unit", Toast.LENGTH_SHORT).show();
            }
        });
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }
    // To handle menu text color group items click event
    public void onColorGroupItemClick(MenuItem item){
        // If red color selected
        if(item.getItemId() == R.id.red){
            // Checked the red color item
            item.setChecked(true);
            // Set the text view text color to red
            mTextView.setTextColor(Color.RED);
        }else if (item.getItemId() == R.id.green){
            // Checked the green color item
            item.setChecked(true);
            // Set the text view text color to green
            mTextView.setTextColor(Color.GREEN);
        }else if (item.getItemId() == R.id.blue){
            // Checked the blue color item
            item.setChecked(true);
            // Set the text view text color to blue
            mTextView.setTextColor(Color.BLUE);
        }else {
            // Do nothing
        }
    }

    // To handle menu text size group items click event
    public void onSizeGroupItemClick(MenuItem item){
        int id = item.getItemId();
        // if large text size selected
        if(id == R.id.large_text){
            // Check the item
            item.setChecked(true);
            // Set the text view text size to large
            mTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,50);
        }else if (id == R.id.medium_text){
            // Check the item
            item.setChecked(true);
            // Set the text view text size to medium
            mTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,30);
        }else if (id == R.id.small_text){
            // Check the item
            item.setChecked(true);
            // Set the text view text size to large
            mTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        }else {
            // Do nothing
        }
    }
*/
    private void constructArray() {
        volumeArray = new TextView[NUM_OF_VOLUME_UNITS];
        volumeArray[0] = findViewById(R.id.teaspoon_text_view_display);
        volumeArray[1] = findViewById(R.id.tablespoon_text_view_display);
        volumeArray[2] = findViewById(R.id.fluid_ounce_text_view_display);
        volumeArray[3] = findViewById(R.id.gill_text_view_display);
        volumeArray[4] = findViewById(R.id.cup_text_view_display);
        volumeArray[5] = findViewById(R.id.pint_text_view_display);
        volumeArray[6] = findViewById(R.id.quart_text_view_display);
        volumeArray[7] = findViewById(R.id.gallon_text_view_display);
        volumeArray[8] = findViewById(R.id.ml_text_view_display);
        volumeArray[9] = findViewById(R.id.l_text_view_display);
        volumeArray[10] = findViewById(R.id.dl_text_view_display);

        massAndWeightArray = new TextView[NUM_OF_MASS_AND_WEIGHT_UNITS];
        massAndWeightArray[0] = findViewById(R.id.pound_text_view_display);
        massAndWeightArray[1] = findViewById(R.id.ounce_text_view_display);
        massAndWeightArray[2] = findViewById(R.id.g_text_view_display);
        massAndWeightArray[3] = findViewById(R.id.mg_text_view_display);
        massAndWeightArray[4] = findViewById(R.id.kg_text_view_display);

        lengthArray = new TextView[NUM_OF_LENGTH_UNITS];
        lengthArray[0] = findViewById(R.id.mm_text_view_display);
        lengthArray[1] = findViewById(R.id.cm_text_view_display);
        lengthArray[2] = findViewById(R.id.m_text_view_display);
        lengthArray[3] = findViewById(R.id.inch_text_view_display);
        lengthArray[4] = findViewById(R.id.feet_text_view_display);

        temperatureArray = new TextView[NUM_OF_TEMPERATURE_UNITS];
        temperatureArray[0] = findViewById(R.id.fahrenheit_text_view_display);
        temperatureArray[1] = findViewById(R.id.celsius_text_view_display);
        temperatureArray[2] = findViewById(R.id.gas_mark_text_view_display);
        temperatureArray[3] = findViewById(R.id.verbal_text_view_display);
    }

    private void constructUnitArray() {

        unitVolumeArray[0] = new Unit(GALLON, 0, 0);
        unitVolumeArray[1] = new Unit(QUART, 0, 0);
        unitVolumeArray[2] = new Unit(PINT, 0, 0);
        unitVolumeArray[3] = new Unit(CUP, 0, 0);
        unitVolumeArray[4] = new Unit(TABLESPOON, 0, 0);
        unitVolumeArray[5] = new Unit(TEASPOON, 0, 0);
        unitVolumeArray[6] = new Unit(ML, 0, 0);
        unitVolumeArray[7] = new Unit(L, 0, 0);
        unitVolumeArray[8] = new Unit(DL, 0, 0);
        unitVolumeArray[9] = new Unit(FLUID_OUNCE, 0, 0);
        unitVolumeArray[10] = new Unit(GILL, 0, 0);

        unitMassAndWeightArray[0] = new Unit(POUND, 0, 0);
        unitMassAndWeightArray[1] = new Unit(OUNCE, 0, 0);
        unitMassAndWeightArray[2] = new Unit(G, 0, 0);
        unitMassAndWeightArray[3] = new Unit(MG, 0, 0);
        unitMassAndWeightArray[4] = new Unit(KG, 0, 0);

        unitLengthArray[0] = new Unit(MM, 0, 0);
        unitLengthArray[1] = new Unit(CM, 0, 0);
        unitLengthArray[2] = new Unit(M, 0, 0);
        unitLengthArray[3] = new Unit(INCH, 0, 0);
        unitLengthArray[4] = new Unit(FEET, 0, 0);

        unitTemperatureArray[0] = new Unit(FAHRENHEIT, 0, 0);
        unitTemperatureArray[1] = new Unit(CELSIUS, 0, 0);
        unitTemperatureArray[2] = new Unit(GAS_MARK, 0, 0);
        unitTemperatureArray[3] = new Unit(VERBAL, 0, 0);

//        unitVolumeArray[0].setConvertRight(4);
//        unitVolumeArray[1].setConvertLeft(1d / 4d);
//        unitVolumeArray[1].setConvertRight(2);
//        unitVolumeArray[2].setConvertLeft(1d / 2d);
//        unitVolumeArray[2].setConvertRight(2);
//        unitVolumeArray[3].setConvertLeft(1d / 2d);
//        unitVolumeArray[3].setConvertRight(16);
//        unitVolumeArray[4].setConvertLeft(1 / 16d);
//        unitVolumeArray[4].setConvertRight(3);
//        unitVolumeArray[5].setConvertLeft(1d / 3d);
//        unitVolumeArray[5].setConvertRight(4.929d);
//        unitVolumeArray[6].setConvertLeft(1d / 4.929d);
//        unitVolumeArray[6].setConvertRight(1d / 1000d);
//        unitVolumeArray[7].setConvertLeft(1000);
//        unitVolumeArray[7].setConvertRight(10);
//        unitVolumeArray[8].setConvertLeft(1d / 10d);
//        unitVolumeArray[9].setConvertRight(1d / 4d);
//        unitVolumeArray[10].setConvertLeft(4);

        unitVolumeArray[0].setFracRight(new Fraction(4,1));
        unitVolumeArray[1].setFracLeft(new Fraction(1, 4));
        unitVolumeArray[1].setFracRight(new Fraction(2,1));
        unitVolumeArray[2].setFracLeft(new Fraction(1,2));
        unitVolumeArray[2].setFracRight(new Fraction(2,1));
        unitVolumeArray[3].setFracLeft(new Fraction(1,2));
        unitVolumeArray[3].setFracRight(new Fraction(16,1));
        unitVolumeArray[4].setFracLeft(new Fraction(1,16));
        unitVolumeArray[4].setFracRight(new Fraction(3,1));
        unitVolumeArray[5].setFracLeft(new Fraction(1,3));
        unitVolumeArray[5].setFracRight(new Fraction(4929,1000));
        unitVolumeArray[6].setFracLeft(new Fraction(1000,4929));
        unitVolumeArray[6].setFracRight(new Fraction(1,1000));
        unitVolumeArray[7].setFracLeft(new Fraction(1000,1));
        unitVolumeArray[7].setFracRight(new Fraction(10,1));
        unitVolumeArray[8].setFracLeft(new Fraction(1,10));
        unitVolumeArray[9].setFracRight(new Fraction(1,4));
        unitVolumeArray[10].setFracLeft(new Fraction(4,1));

//        unitMassAndWeightArray[0].setConvertRight(16);
//        unitMassAndWeightArray[1].setConvertLeft(1d / 16d);
//        unitMassAndWeightArray[1].setConvertRight(28.349523125);
//        unitMassAndWeightArray[2].setConvertLeft(1d / 28.349523125d);
//        unitMassAndWeightArray[2].setConvertRight(1000);
//        unitMassAndWeightArray[3].setConvertLeft(1d / 1000d);
//        unitMassAndWeightArray[3].setConvertRight(1d / 1000000d);
//        unitMassAndWeightArray[4].setConvertLeft(1000000);

        unitMassAndWeightArray[0].setFracRight(new Fraction(16, 1));
        unitMassAndWeightArray[1].setFracLeft(new Fraction (1, 16));
        unitMassAndWeightArray[1].setFracRight(new Fraction(28349523125L, 1000000000L));
        unitMassAndWeightArray[2].setFracLeft(new Fraction (1000000000L, 28349523125L));
        unitMassAndWeightArray[2].setFracRight(new Fraction(1000, 1));
        unitMassAndWeightArray[3].setFracLeft(new Fraction (1, 1000));
        unitMassAndWeightArray[3].setFracRight(new Fraction(1, 1000000));
        unitMassAndWeightArray[4].setFracLeft(new Fraction (1000000, 1));

//        unitLengthArray[0].setConvertRight(1d / 10d);
//        unitLengthArray[1].setConvertLeft(10);
//        unitLengthArray[1].setConvertRight(1d / 100d);
//        unitLengthArray[2].setConvertLeft(100);
//        unitLengthArray[2].setConvertRight(1d / 0.0254d);
//        unitLengthArray[3].setConvertLeft(0.0254);
//        unitLengthArray[3].setConvertRight(1d / 12d);
//        unitLengthArray[4].setConvertLeft(12);

        unitLengthArray[0].setFracRight(new Fraction(1,10));
        unitLengthArray[1].setFracLeft(new Fraction(10,1));
        unitLengthArray[1].setFracRight(new Fraction(1,100));
        unitLengthArray[2].setFracLeft(new Fraction(100,1));
        unitLengthArray[2].setFracRight(new Fraction(10000,254));
        unitLengthArray[3].setFracLeft(new Fraction(254,10000));
        unitLengthArray[3].setFracRight(new Fraction(1,12));
        unitLengthArray[4].setFracLeft(new Fraction(12,1));




//        unitTemperatureArray[0].setConvertRight(-17.222222222);
//        unitTemperatureArray[1].setConvertLeft(0.029585798816568);
//        unitTemperatureArray[1].setConvertRight();
    }

    private void convertVolume() {
        /*
          0:  gallon
          1:  quart
          2:  pint
          3:  cup
          4:  tablespoon
          5:  teaspoon
          6:  ml
          7:  l
          8:  dl
          9:  fluid ounce
          10: gill
         */
        Fraction[] resultsArray = new Fraction[NUM_OF_VOLUME_UNITS];

        int temp;
        switch (index) {
            case 0:
                temp = 5;
                break;
            case 1:
                temp = 4;
                break;
            case 2:
                temp = 9;
                break;
            case 3:
                temp = 10;
                break;
            case 4:
                temp = 3;
                break;
            case 5:
                temp = 2;
                break;
            case 6:
                temp = 1;
                break;
            case 7:
                temp = 0;
                break;
            case 8:
                temp = 6;
                break;
            case 9:
                temp = 7;
                break;
            case 10:
                temp = 8;
                break;
            default:
                temp = 0;
        }
        resultsArray[temp] = new Fraction(userInputEditText.getText().toString());

        if (temp == 10 | temp == 9) {
            if (temp == 10) {
                resultsArray[9] = resultsArray[10].multiply(unitVolumeArray[10].getFracLeft());
            } else {
                resultsArray[10] = resultsArray[9].multiply(unitLengthArray[9].getFracRight());
            }

            resultsArray[5] = resultsArray[9].multiply(new Fraction(6,1));

            for (int i = 6; i < 9; i++) {
                resultsArray[i] = resultsArray[i - 1].multiply(unitVolumeArray[i - 1].getFracRight());
            }
            for (int i = 4; i > -1; i--) {
                resultsArray[i] = resultsArray[i + 1].multiply(unitVolumeArray[i + 1].getFracLeft());
            }
        } else {
            if (temp > 5) {
                for (int i = temp - 1; i > -1; i--) {
                    resultsArray[i] = resultsArray[i + 1].multiply(unitVolumeArray[i + 1].getFracLeft());

                }
                for (int i = temp + 1; i < NUM_OF_VOLUME_UNITS; i++) {
                    if (i == 9) {
                        resultsArray[i] = resultsArray[5].divide(new Fraction(6, 1));
                    } else {
                        resultsArray[i] = resultsArray[i - 1].multiply(unitVolumeArray[i - 1].getFracRight());
                    }
                }
            } else {
                for (int i = temp + 1; i < NUM_OF_VOLUME_UNITS; i++) {
                    if (i == 9) {
                        resultsArray[i] = resultsArray[5].divide(new Fraction(6, 1));
                    } else {
                        resultsArray[i] = resultsArray[i - 1].multiply(unitVolumeArray[i - 1].getFracRight());
                    }
                }
                for (int i = temp - 1; i > -1; i--) {
                    resultsArray[i] = resultsArray[i + 1].multiply(unitVolumeArray[i + 1].getFracLeft());
                }
            }
        }

        setVolumeNewDisplay(resultsArray);
    }

    private void convertMassAndWeight() {
        Fraction[] resultsArray = new Fraction[NUM_OF_MASS_AND_WEIGHT_UNITS];

        int temp;
        switch (index) {
            case 0:
                temp = 0;
                break;
            case 1:
                temp = 1;
                break;
            case 2:
                temp = 2;
                break;
            case 3:
                temp = 3;
                break;
            case 4:
                temp = 4;
                break;
            default:
                temp = 0;
        }
        resultsArray[temp] = new Fraction(userInputEditText.getText().toString());

        for (int i = index + 1; i < NUM_OF_MASS_AND_WEIGHT_UNITS; i++) {
            resultsArray[i] = resultsArray[i - 1].multiply(unitMassAndWeightArray[i - 1].getFracRight());
        }
        for (int i = index - 1; i > -1; i--) {
            resultsArray[i] = resultsArray[i + 1].multiply(unitMassAndWeightArray[i + 1].getFracLeft());
        }

        setMassAndWeightNewDisplay(resultsArray);
    }

    private void convertLength() {
        Fraction[] resultsArray = new Fraction[NUM_OF_LENGTH_UNITS];

        int temp;
        switch (index) {
            case 0:
                temp = 0;
                break;
            case 1:
                temp = 1;
                break;
            case 2:
                temp = 2;
                break;
            case 3:
                temp = 3;
                break;
            case 4:
                temp = 4;
                break;
            default:
                temp = 0;
        }

        resultsArray[temp] = new Fraction(userInputEditText.getText().toString());

        for (int i = index + 1; i < NUM_OF_LENGTH_UNITS; i++) {
            resultsArray[i] = resultsArray[i - 1].multiply(unitLengthArray[i - 1].getFracRight());
        }
        for (int i = index - 1; i > -1; i--) {
            resultsArray[i] = resultsArray[i + 1].multiply(unitLengthArray[i + 1].getFracLeft());
        }

        setLengthNewDisplay(resultsArray);
    }

    private void convertTemperature() {

    }

    private void setVolumeNewDisplay(Fraction[] resultsArray) {
        volumeArray[0].setText(resultsArray[5].toString() + "t");
        volumeArray[1].setText(resultsArray[4].toString() + "T");
        volumeArray[2].setText(resultsArray[9].toString() + "fl");
        volumeArray[3].setText(resultsArray[10].toString() + "gill(s)");
        volumeArray[4].setText(resultsArray[3].toString() + "cup(s)");
        volumeArray[5].setText(resultsArray[2].toString() + "pint(s)");
        volumeArray[6].setText(resultsArray[1].toString() + "qts");
        volumeArray[7].setText(resultsArray[0].toString() + "gal");
        volumeArray[8].setText(resultsArray[6].toString() + "ml");
        volumeArray[9].setText(resultsArray[7].toString() + "l");
        volumeArray[10].setText(resultsArray[8].toString() + "dl");
    }

    private void setMassAndWeightNewDisplay(Fraction[] resultsArray) {
        massAndWeightArray[0].setText(resultsArray[0].toString() + "lbs");
        massAndWeightArray[1].setText(resultsArray[1].toString() + "oz");
        massAndWeightArray[2].setText(resultsArray[2].toString() + "g");
        massAndWeightArray[3].setText(resultsArray[3].toString() + "mg");
        massAndWeightArray[4].setText(resultsArray[4].toString() + "kg");
    }

    private void setLengthNewDisplay(Fraction[] resultsArray) {
        lengthArray[0].setText(resultsArray[0].toString() + "mm");
        lengthArray[1].setText(resultsArray[1].toString() + "cm");
        lengthArray[2].setText(resultsArray[2].toString() + "m");
        lengthArray[3].setText(resultsArray[3].toString() + "inch");
        lengthArray[4].setText(resultsArray[4].toString() + "ft");
    }

    private void setTemperatureNewDisplay(Double[] resultsArray) {
        temperatureArray[0].setText(resultsArray[0] + "F");
        temperatureArray[1].setText(resultsArray[1] + "C");
        temperatureArray[2].setText(resultsArray[2] + "");
        temperatureArray[3].setText(resultsArray[3] + "");
    }

    /**
     * Checks if the user is converting a volume unit
     *
     * @return true if baseUnit is a volume unit, else false
     */
    private boolean isVolume() {
        return baseUnit.equals(TEASPOON) || baseUnit.equals(TABLESPOON) ||
                baseUnit.equals(FLUID_OUNCE) || baseUnit.equals(GILL) ||
                baseUnit.equals(CUP) || baseUnit.equals(PINT) ||
                baseUnit.equals(QUART) || baseUnit.equals(GALLON) ||
                baseUnit.equals(ML) || baseUnit.equals(L) ||
                baseUnit.equals(DL);
    }

    /**
     * Checks if the user is converting a Mass and Weight unit
     *
     * @return true if baseUnit is a Mass and Weight unit, else false
     */
    private boolean isMassAndWeight() {
        return baseUnit.equals(POUND) || baseUnit.equals(OUNCE) ||
                baseUnit.equals(G) || baseUnit.equals(MG) ||
                baseUnit.equals(KG);
    }

    /**
     * Checks if the user is converting a length unit
     *
     * @return true if baseUnit is a length unit, else false
     */
    private boolean isLength() {
        return baseUnit.equals(MM) || baseUnit.equals(CM) ||
                baseUnit.equals(M) || baseUnit.equals(INCH) ||
                baseUnit.equals(FEET);
    }

    /**
     * Checks if the user is converting a length unit
     *
     * @return true if baseUnit is a length unit, else false
     */
    private boolean isTemperature() {
        return baseUnit.equals(FAHRENHEIT) || baseUnit.equals(CELSIUS) ||
                baseUnit.equals(GAS_MARK) || baseUnit.equals(VERBAL);
    }

    /**
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        /* Adding child data */
        listDataHeader.add("Volume");
        listDataHeader.add("Mass and Weight");
        listDataHeader.add("Length");
        listDataHeader.add("Temperature");

        /* Adding child data */
        List<String> volumeList = new ArrayList<>();
        volumeList.add(TEASPOON);
        volumeList.add(TABLESPOON);
        volumeList.add(FLUID_OUNCE);
        volumeList.add(GILL);
        volumeList.add(CUP);
        volumeList.add(PINT);
        volumeList.add(QUART);
        volumeList.add(GALLON);
        volumeList.add(ML);
        volumeList.add(L);
        volumeList.add(DL);

        List<String> massAndWeightList = new ArrayList<>();
        massAndWeightList.add(POUND);
        massAndWeightList.add(OUNCE);
        massAndWeightList.add(MG);
        massAndWeightList.add(G);
        massAndWeightList.add(KG);

        List<String> lengthList = new ArrayList<>();
        lengthList.add(MM);
        lengthList.add(CM);
        lengthList.add(M);
        lengthList.add(INCH);
        lengthList.add(FEET);

        List<String> TemperatureList = new ArrayList<>();
        TemperatureList.add(FAHRENHEIT);
        TemperatureList.add(CELSIUS);
        TemperatureList.add(GAS_MARK);
        TemperatureList.add(VERBAL);

        listDataChild.put(listDataHeader.get(0), volumeList);   //  Header, Child data
        listDataChild.put(listDataHeader.get(1), massAndWeightList);
        listDataChild.put(listDataHeader.get(2), lengthList);
        listDataChild.put(listDataHeader.get(3), TemperatureList);
    }
}
