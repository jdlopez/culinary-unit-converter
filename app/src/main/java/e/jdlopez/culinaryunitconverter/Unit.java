package e.jdlopez.culinaryunitconverter;

public class Unit {

    /**
     * Name of the unit
     */
    private String name;

    /**
     * multiplier to get from the current unit to the next(on the array)
     */
    private long convertRight;

    /**
     * multiplier to get from the current unit to the one before(on the array)
     */
    private long convertLeft;

    private Fraction fracRight;
    private Fraction fracLeft;

    /*  gallon - quart - pint - cup - tablespoon - teaspoon - ml - l - dl */

    public Unit(String name, long right, long left) {
        this.name = name;

        convertRight = right;
        convertLeft = left;

        fracRight = new Fraction(convertRight);
        fracLeft = new Fraction(convertLeft);
    }

    public void setConvertLeft(long convertLeft) {
        this.convertLeft = convertLeft;
//        setFracLeft(new Fraction(this.convertLeft));
    }

    public void setConvertRight(long convertRight) {
        this.convertRight = convertRight;
//        setFracRight(new Fraction(this.convertRight));
    }

    public void setFracLeft(Fraction fracLeft) {
        this.fracLeft = fracLeft;
    }

    public void setFracRight(Fraction fracRight) {
        this.fracRight = fracRight;
    }

    public double getConvertLeft() {
        return convertLeft;
    }

    public double getConvertRight() {
        return convertRight;
    }

    public String getName(){
        return name;
    }

    public Fraction getFracRight(){
        return this.fracRight;
    }

    public Fraction getFracLeft(){
        return this.fracLeft;
    }
}
